import { Component, OnInit } from '@angular/core';

import { routerAnimation } from '../animations/routerAnimation';

@Component({
  selector: 'mill-credits',
  templateUrl: './credits.component.html',
  styleUrls: ['./credits.component.scss'],
  animations: [
    routerAnimation
  ],
})
export class CreditsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
