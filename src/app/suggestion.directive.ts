import { Directive, Input, ElementRef, Renderer } from '@angular/core';
import { Suggestion } from './mill-game/suggestion';

@Directive({
  selector: '[millSuggestion]'
})
export class SuggestionDirective {
  @Input() millSuggestion: Suggestion;

  constructor(private el: ElementRef, public renderer: Renderer) {
  }

  ngOnInit() {
    let topProp: number = this.millSuggestion.coordinate.z * 15;
    let leftProp: number = this.millSuggestion.coordinate.z * 15;

    let multiplier = 50;
    if (this.millSuggestion.coordinate.z == 2) {
      multiplier = 20;
    } else if (this.millSuggestion.coordinate.z == 1) {
      multiplier = 35;
    }
    leftProp += this.millSuggestion.coordinate.x * multiplier;
    topProp += this.millSuggestion.coordinate.y * multiplier;
    this.renderer.setElementStyle(this.el.nativeElement, 'top', topProp + '%');
    this.renderer.setElementStyle(this.el.nativeElement, 'left', leftProp + '%');
    this.renderer.setElementStyle(this.el.nativeElement, 'border-color', this.millSuggestion.player.color);

    
    if (this.millSuggestion.coordinate.x == 0) {
      this.renderer.setElementClass(this.el.nativeElement, 'first-col', true);
    } else if (this.millSuggestion.coordinate.x == 2) {
      this.renderer.setElementClass(this.el.nativeElement, 'last-col', true);
    }

    if (this.millSuggestion.coordinate.y == 0) {
      this.renderer.setElementClass(this.el.nativeElement, 'first-row', true);
    } else if (this.millSuggestion.coordinate.y == 2) {
      this.renderer.setElementClass(this.el.nativeElement, 'last-row', true);
    }

  }
}
