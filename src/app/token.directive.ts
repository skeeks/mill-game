
import { Directive, ElementRef, Input, Renderer, OnChanges, HostListener, DoCheck } from '@angular/core';
import { MillToken } from './mill-game/mill-token';
import { } from '@angular/core/src/metadata/lifecycle_hooks';

@Directive({
  selector: '[millToken]'
})
export class TokenDirective {
  @Input() millToken: MillToken;

  constructor(private el: ElementRef, public renderer: Renderer) {
  }

  ngOnInit() {
    this.apply();
  }
  //not sure if this is good, but it works
  ngDoCheck() {
    this.apply();
  }

  apply() {
    let topProp: number = this.millToken.coordinate.z * 15;
    let leftProp: number = this.millToken.coordinate.z * 15;

    let multiplier = 50;
    if (this.millToken.coordinate.z == 2) {
      multiplier = 20;
    } else if (this.millToken.coordinate.z == 1) {
      multiplier = 35;
    }
    leftProp += this.millToken.coordinate.x * multiplier;
    topProp += this.millToken.coordinate.y * multiplier;
    this.renderer.setElementStyle(this.el.nativeElement, 'top', topProp + '%');
    this.renderer.setElementStyle(this.el.nativeElement, 'left', leftProp + '%');

    if (this.millToken.coordinate.x == 0) {
      // this.renderer.setElementClass(this.el.nativeElement, 'first-col', true);
      this.el.nativeElement.classList.remove('last-col');
      this.renderer.setElementClass(this.el.nativeElement, "first-col", true);
    } else if (this.millToken.coordinate.x == 2) {
      // this.renderer.setElementClass(this.el.nativeElement, 'last-col', true);
      this.el.nativeElement.classList.remove('first-col');
      this.renderer.setElementClass(this.el.nativeElement, "last-col", true);
    } else {
      this.el.nativeElement.classList.remove('first-col');
      this.el.nativeElement.classList.remove('last-col');
    }

    if (this.millToken.coordinate.y == 0) {
      this.el.nativeElement.classList.remove('last-row');
      this.renderer.setElementClass(this.el.nativeElement, "first-row", true);
    } else if (this.millToken.coordinate.y == 2) {
      this.el.nativeElement.classList.remove('first-row');
      this.renderer.setElementClass(this.el.nativeElement, "last-row", true);
    } else {
      this.el.nativeElement.classList.remove('first-row');
      this.el.nativeElement.classList.remove('last-row');
      // this.renderer.setElementClass(this.el.nativeElement, this.el.nati)
    }

    if (this.millToken.player) {
      this.renderer.setElementStyle(this.el.nativeElement, 'background-color', this.millToken.player.color);
    }
  }
}
