import {
    trigger,
    state,
    style,
    animate,
    transition
} from '@angular/animations';

export const tokenAppearAnimation =
    trigger('tokenAppearAnimation', [
        // end state styles for route container (host)
        transition(':enter', [
            style({
                transform: 'scale(0)'
            }),
            animate('0.6s ease-in-out', style({
                transform: 'scale(1)'
            }))
        ]),
        transition(':leave', [
            style({
                transform: 'scale(1)'
            }),
            animate('0.3s ease-in-out', style({ 'transform': 'scale(0)' }))
        ]),
    ]);