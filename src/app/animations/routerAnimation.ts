import {
    trigger,
    state,
    style,
    animate,
    transition
} from '@angular/animations';

export const routerAnimation =
    trigger('routerAnimation', [
        transition(':enter', [
            style({
                'transform': 'scale(0.0)'
            }),
            animate('0.6s ease-in-out', style({ 'transform': 'scale(1.0)' }))
        ]),
        transition(':leave', [
            animate('0.3s ease-in-out', style({ 'transform': 'scale(1.0)' }))
        ]),
    ]);