import { Injectable } from '@angular/core';
import { Suggestion } from './suggestion';
import { MillToken } from './mill-token';
import { Player } from './player';
import { GameInputData } from './game-input-data';
import { Coordinate } from './coordinate';
import { Mode } from './mode';
import { SuggestionType } from './suggestion-type';
import { TokenBoard } from './token-board';

@Injectable()
export class MillService {

  private board: TokenBoard;

  private nextPlayer: Player;

  private currentPlayer: Player;

  private gameFinished: boolean;

  // --------------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------------
  // Initialization And Start Game Methods ------------------------------------------------
  constructor() {
    this.gameFinished = false;
  }

  /**
   * Start the MillGame with the passed InputData
   * @param inputData 
   */
  public start(inputData: GameInputData): Suggestion[] {
    this.preparePlayers(inputData);

    this.prepareBoardForGame(inputData);

    return this.preparePlaceTurn();
  }

  /**
   * initialize and prepare the player objects for the game
   * @param inputData 
   */
  private preparePlayers(inputData: GameInputData) {
    this.gameFinished = false;
    if (inputData.players.length != 2) {
      throw new Error('2 Players are required');
    }
    this.currentPlayer = inputData.players[0];
    this.nextPlayer = inputData.players[1];
  }

  /**
   * internal function to 
   * @param inputData 
   */
  private prepareBoardForGame(inputData: GameInputData) {
    this.board = new TokenBoard();
  }


  /**
   * Executes the next turn or place new suggestions
   * @param suggestion the clicked suggestion
   */
  public executeNextTurn(suggestion: Suggestion): Suggestion[] {
    // execute the suggestion
    switch (suggestion.type) {
      case SuggestionType.PLACE: {
        this.executePlaceTurn(suggestion);
        break;
      }
      case SuggestionType.MOVE_CHOOSE: {
        let token = this.executeMoveChooseTurn(suggestion);
        return this.prepareMoveTurn(token);
      }
      case SuggestionType.MOVE: {
        this.executeMoveTurn(suggestion);
        break;
      }
      case SuggestionType.JUMP_CHOOSE: {
        let token = this.executeJumpChooseTurn(suggestion);
        return this.prepareJumpTurn(token);
      }
      case SuggestionType.JUMP: {
        this.executeJumpTurn(suggestion);
        break;
      }
      case SuggestionType.REMOVE: {
        this.executeRemoveTurn(suggestion);
        break;
      }
    }
    // check if a new mill happened
    if (suggestion.type != SuggestionType.REMOVE && this.board.isMill(suggestion.coordinate)) {
      // if yes, prepare remove turn for the same player and return.
      let suggestions = this.prepareRemoveTurn();
      if (suggestions.length > 0) {
        return suggestions;
      }
    }
    // determine player mode, it could have changed
    this.currentPlayer.mode = this.determinePlayerMode(this.currentPlayer);
    // the next player is now on turn
    this.changePlayer();
    // determine player mode, it could have changed
    this.currentPlayer.mode = this.determinePlayerMode(this.currentPlayer);

    // prepare the next turn
    switch (this.currentPlayer.mode) {
      case Mode.PLACE: {
        return this.preparePlaceTurn();
      }
      case Mode.MOVE: {
        return this.prepareMoveChooseTurn();
      }
      case Mode.JUMP: {
        return this.prepareJumpChooseTurn();
      }
    }
  }

  // --------------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------------
  // Place --------------------------------------------------------------------------------
  /**
   * execute the place turn suggestion
   * @param suggestion 
   */
  public executePlaceTurn(suggestion: Suggestion) {
    if (this.getTokenAt(suggestion.coordinate) != null) {
      throw new Error('cannnot place token on another token');
    }

    let token = new MillToken(suggestion.player, suggestion.coordinate);
    this.board.put(token);

    this.currentPlayer.placedTokens++;
    this.currentPlayer.remainingTokens++;
  }

  /**
   * return Suggestions for a place turn
   */
  public preparePlaceTurn(): Suggestion[] {
    let that = this;
    let suggestions: Array<Suggestion> = new Array();

    this.loopThroughPositions(function (coordinate: Coordinate, token: MillToken) {
      if (token === null) {
        suggestions.push(new Suggestion(coordinate, that.currentPlayer, SuggestionType.PLACE));
      }
    });
    return suggestions;
  }

  // --------------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------------
  // Move ---------------------------------------------------------------------------------

  /**
   * return suggestions for a move choose turn
   */
  public prepareMoveChooseTurn(): Suggestion[] {
    let that = this;
    let suggestions = new Array<Suggestion>();
    this.getTokensOfPlayer(this.currentPlayer).forEach(function (token: MillToken) {
      if (that.board.isMovable(token)) {
        suggestions.push(new Suggestion(token.coordinate, that.currentPlayer, SuggestionType.MOVE_CHOOSE));
      }
    });
    // if no tokens can be moved, the player has lost
    if (suggestions.length == 0) {
      this.finishGame(this.nextPlayer);
      return [];
    }
    return suggestions;
  }

  public executeMoveChooseTurn(suggestion: Suggestion): MillToken {
    return this.getTokenAt(suggestion.coordinate);
  }

  /**
   * return suggestions for move turn
   * @param token 
   */
  public prepareMoveTurn(token: MillToken): Suggestion[] {
    let that = this;
    let freePlaces = this.board.getFreeNeighbourPlaces(token);
    let suggestions = new Array<Suggestion>();
    freePlaces.forEach(function (coordinate: Coordinate) {
      suggestions.push(new Suggestion(coordinate, that.currentPlayer, SuggestionType.MOVE, token));
    });
    let additionalSuggestions = this.prepareMoveChooseTurn();
    return suggestions.concat(additionalSuggestions);
  }

  /**
   * execute the move turn suggestion
   * @param suggestion 
   */
  public executeMoveTurn(suggestion: Suggestion) {
    if (this.getTokenAt(suggestion.coordinate) != null) {
      throw new Error('cannnot move token to another token');
    }
    let token = suggestion.origin;
    this.board.remove(token.coordinate);
    token.coordinate = suggestion.coordinate;
    this.board.put(token);

  }

  // --------------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------------
  // Jump -----------------------------------------------------------------------------------

  /**
   * return suggestions for jump turn
   */
  public prepareJumpChooseTurn(): Suggestion[] {
    let that = this;
    let suggestions = new Array<Suggestion>();
    this.getTokensOfPlayer(this.currentPlayer).forEach(function (token: MillToken) {
      suggestions.push(new Suggestion(token.coordinate, that.currentPlayer, SuggestionType.JUMP_CHOOSE));
    });
    return suggestions;
  }

  public executeJumpChooseTurn(suggestion: Suggestion): MillToken {
    return this.getTokenAt(suggestion.coordinate);
  }

  /**
   * return suggestions for jumping 
   * @param originToken 
   */
  public prepareJumpTurn(originToken: MillToken): Suggestion[] {
    let that = this;
    let suggestions = new Array<Suggestion>();
    this.loopThroughPositions(function (coordinate: Coordinate, token: MillToken) {
      if (token === null) {
        suggestions.push(new Suggestion(coordinate, that.currentPlayer, SuggestionType.JUMP, originToken));
      }
    });
    let additionalSuggestions = this.prepareJumpChooseTurn();
    return suggestions.concat(additionalSuggestions);
  }

  /**
   * execute jump
   * @param suggestion 
   */
  public executeJumpTurn(suggestion: Suggestion) {
    if (this.getTokenAt(suggestion.coordinate) != null) {
      throw new Error('cannnot jump token to another token');
    }
    let token = suggestion.origin;
    this.board.remove(token.coordinate);
    token.coordinate = suggestion.coordinate;
    this.board.put(token);
  }

  // --------------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------------
  // Remove ------------------------------------------------------------------------------------

  /**
   * return suggestions for removing tokens
   */
  public prepareRemoveTurn(): Suggestion[] {
    let that = this;
    let suggestions = new Array<Suggestion>();
    this.getTokensOfPlayer(this.nextPlayer).forEach(function (token: MillToken) {
      if (!that.board.isMill(token.coordinate)) {
        suggestions.push(new Suggestion(token.coordinate, that.currentPlayer, SuggestionType.REMOVE));
      }
    });
    return suggestions;
  }

  /**
   * remove the chosen token
   * @param suggestion 
   */
  public executeRemoveTurn(suggestion: Suggestion) {
    if (this.board.isMill(suggestion.coordinate)) {
      throw new Error("cannot remove a token out of a mill");
    }
    let token = this.board.get(suggestion.coordinate);
    this.board.remove(suggestion.coordinate);
    token.player.remainingTokens--;
    if (token.player.placedTokens == 9 && token.player.remainingTokens < 3) {
      this.finishGame(this.currentPlayer);
    }
  }

  // Helper Methods -------------------------------------------------------------------------------
  /**
   * set the game to finished and set the winner
   * @param winner
   */
  public finishGame(winner: Player) {
    this.gameFinished = true;
    this.currentPlayer = winner;
  }

  /**
   * returns true if game is finished
   */
  public isGameFinished(): boolean {
    return this.gameFinished;
  }

  /**
   * returns the winner of the game or null, if the game is not finished yet
   */
  public getWinner(): Player {
    if (this.isGameFinished()) {
      return this.currentPlayer;
    } else {
      return null;
    }
  }

  /**
   * get all remaining tokens of a player
   * @param player 
   */
  private getTokensOfPlayer(player: Player): Array<MillToken> {
    let list = new Array<MillToken>();
    this.board.getTokens().forEach(function (token: MillToken) {
      if (token.player === player) {
        list.push(token);
      }
    });
    return list;
  }

  /**
   * change player
   */
  private changePlayer(): Player {
    let tmpPlayer = this.currentPlayer;
    this.currentPlayer = this.nextPlayer;
    this.nextPlayer = tmpPlayer;
    return this.currentPlayer;
  }

  /**
   * get all tokens on the board.
   */
  public getTokens(): Array<MillToken> {
    return this.board.getTokens();
  }


  /**
   * find out in which mode the passed player is
   * @param player 
   */
  private determinePlayerMode(player: Player): Mode {
    if (player.placedTokens < 9) {
      return Mode.PLACE;
    } else {
      if (player.remainingTokens > 3) {
        return Mode.MOVE;
      } else {
        return Mode.JUMP;
      }
    }
  }

  /**
   * return the token at the coordinate or null, if no token is there.
   * @param coordinate 
   */
  public getTokenAt(coordinate: Coordinate): MillToken {
    let token = this.board.get(coordinate);

    if (token) {
      return token;
    } else {
      return null;
    }
  }

  /**
   * return true if no token is at the passed coordinate
   * @param coordinate 
   */
  public isEmpty(coordinate: Coordinate): boolean {
    return this.board.get(coordinate) === null;
  }

  /**
   * execute callback for each valid mill position (empty places too)
   * @param callback the coordinate and token are passed to the callback
   */
  private loopThroughPositions(callback: (coordinate: Coordinate, token: MillToken) => void) {
    for (var z = 0; z <= 2; z++) {
      for (var y = 0; y <= 2; y++) {
        for (var x = 0; x <= 2; x++) {
          if (!(x === 1 && y == 1)) {
            let coordinate = new Coordinate(x, y, z);
            let token = this.getTokenAt(coordinate);
            callback(coordinate, token);
          }
        }
      }
    }
  }
}
