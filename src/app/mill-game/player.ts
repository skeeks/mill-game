import { Mode } from "./mode";

export class Player {
    public mode: Mode = Mode.PLACE;

    public placedTokens = 0;
    public remainingTokens = 0;

    constructor(public color: string, public name : string) { }

    public equal(other: Player) : boolean {
        return other && this.mode == other.mode &&
            this.placedTokens == other.placedTokens &&
            this.remainingTokens == other.remainingTokens &&
            this.color === other.color &&
            this.name === other.name;
    }
}