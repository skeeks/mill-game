export enum SuggestionType {
    PLACE,
    MOVE_CHOOSE,
    MOVE,
    JUMP_CHOOSE,
    JUMP,
    REMOVE
}