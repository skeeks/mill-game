import { Coordinate } from "./coordinate";
import { MillToken } from "./mill-token";


export class TokenBoard {
    private internalBoard = {};


    public put(token: MillToken) {
        if (token == null) return;
        this.internalBoard[this.createKey(token.coordinate)] = token;
    }

    public remove(coordinate: Coordinate) {
        if (coordinate == null) return;
        delete this.internalBoard[this.createKey(coordinate)];
    }

    public get(coordinate: Coordinate) {
        if (coordinate == null) return null;
        return this.internalBoard[this.createKey(coordinate)];
    }

    public getTokens(): any {
        return Object.values(this.internalBoard).sort(function(a : MillToken,b:MillToken){
            return a.id - b.id;
        });
    }

    private createKey(coordinate: Coordinate) {
        return coordinate.x + "/" + coordinate.y + "/" + coordinate.z;
    }

    public neighbours(token: MillToken): MillToken[] {
        let neighbours = new Array<MillToken>();
        let left = this.get(token.coordinate.left());
        if (left) {
            neighbours.push(left);
        }
        let right = this.get(token.coordinate.right());
        if (right) {
            neighbours.push(right);
        }
        let up = this.get(token.coordinate.up());
        if (up) {
            neighbours.push(up);
        }
        let down = this.get(token.coordinate.down());
        if (down) {
            neighbours.push(down);
        }

        return neighbours;
    }
    public hasNeighbours(): boolean {
        return this.neighbours.length > 0;
    }

    public isMovable(token: MillToken): boolean {
        return this.getFreeNeighbourPlaces(token).length > 0;
    }

    public getFreeNeighbourPlaces(token: MillToken): Coordinate[] {
        let freePlaces = new Array<Coordinate>();
        let left = token.coordinate.left();
        if (left && this.get(left) == null) {
            freePlaces.push(left);
        }

        let right = token.coordinate.right();
        if (right && this.get(right) == null) {
            freePlaces.push(right);
        }

        let up = token.coordinate.up();
        if (up && this.get(up) == null) {
            freePlaces.push(up);
        }

        let down = token.coordinate.down();
        if (down && this.get(down) == null) {
            freePlaces.push(down);
        }

        return freePlaces;
    }

    /**
     * returns true if the specified coordinate is a mill
     * @param base the coordinate
     */
    public isMill(base: Coordinate): boolean {
        if (!base) {
            return false;
        }
        let basePlayer = this.get(base).player;

        // to the left
        let left = this.get(base.left());
        let leftLeft = left ? this.get(base.left().left()) : null;
        if (leftLeft && left && basePlayer === leftLeft.player && basePlayer === left.player) {
            return true;
        }

        // to the right
        let right = this.get(base.right());
        let rightRight = right ? this.get(base.right().right()) : null;
        if (rightRight && right && basePlayer === rightRight.player && basePlayer === right.player) {
            return true;
        }

        // to the up
        let up = this.get(base.up());
        let upUp = up ? this.get(base.up().up()) : null;
        if (upUp && up && basePlayer === upUp.player && basePlayer === up.player) {
            return true;
        }

        // to the down
        let down = this.get(base.down());
        let downDown = down ? this.get(base.down().down()) : null;
        if (downDown && down && basePlayer === downDown.player && basePlayer === down.player) {
            return true;
        }

        // horizontal center
        if (right && left && basePlayer === right.player && basePlayer === left.player) {
            return true;
        }
        //vertical center
        if (up && down && basePlayer === up.player && basePlayer === down.player) {
            return true;
        }

        return false;
    }
}