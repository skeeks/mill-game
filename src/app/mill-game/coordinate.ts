export class Coordinate {
    constructor(public x: number, public y: number, public z: number) { }

    public left(): Coordinate {
        let coordinate = new Coordinate(this.x, this.y, this.z);
        if (coordinate.y == 1) {
            if (coordinate.x == 0 && coordinate.z != 0) {
                coordinate.z--;
            } else if (coordinate.x == 2 && coordinate.z != 2) {
                coordinate.z++;
            } else {
                return null;
            }
        } else {
            if (coordinate.x > 0) {
                coordinate.x--;
            } else {
                return null;
            }
        }

        return coordinate;
    }

    public right(): Coordinate {
        let coordinate = new Coordinate(this.x, this.y, this.z);
        if (coordinate.y == 1) {
            if (coordinate.x == 0 && coordinate.z != 2) {
                coordinate.z++;
            } else if (coordinate.x == 2 && coordinate.z != 0) {
                coordinate.z--;
            } else {
                return null;
            }
        } else {
            if (coordinate.x < 2) {
                coordinate.x++;
            } else {
                return null;
            }
        }

        return coordinate;
    }

    public up(): Coordinate {
        let coordinate = new Coordinate(this.x, this.y, this.z);
        if (coordinate.x == 1) {
            if (coordinate.y == 0 && coordinate.z != 0) {
                coordinate.z--;
            } else if (coordinate.y == 2 && coordinate.z != 2) {
                coordinate.z++;
            } else {
                return null;
            }
        } else {
            if (coordinate.y > 0) {
                coordinate.y--;
            } else {
                return null;
            }
        }

        return coordinate;
    }

    public down(): Coordinate {
        let coordinate = new Coordinate(this.x, this.y, this.z);
        if (coordinate.x == 1) {
            if (coordinate.y == 0 && coordinate.z != 2) {
                coordinate.z++;
            } else if (coordinate.y == 2 && coordinate.z != 0) {
                coordinate.z--;
            } else {
                return null;
            }
        } else {
            if (coordinate.y < 2) {
                coordinate.y++;
            } else {
                return null;
            }
        }

        return coordinate;
    }

    public equal(other: Coordinate): boolean {
        return other && this.x == other.x && this.y == other.y && this.z == other.z;
    }
}