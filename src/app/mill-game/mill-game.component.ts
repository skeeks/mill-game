import { MillToken } from './mill-token';
import { Coordinate } from './coordinate';
import { Component, OnInit, Output, IterableDiffers, IterableDiffer, ChangeDetectorRef } from '@angular/core';
import { Player } from './player';
import { MillService } from './mill.service';
import { Suggestion } from './suggestion';
import { routerAnimation } from '../animations/routerAnimation';
import { tokenAppearAnimation } from '../animations/tokenAppearAnimation';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Observable } from 'rxjs';

@Component({
  selector: 'mill-mill-game',
  templateUrl: './mill-game.component.html',
  styleUrls: ['./mill-game.component.scss'],
  animations: [
    routerAnimation,
    trigger('disappearAnimation', [
      transition(':leave', [
        style({'transform': 'scale(1.3)'}),
        animate('0.3s ease-in-out', style({ 'transform': 'scale(0)' }))
      ]),
    ])
  ],
})
export class MillGameComponent implements OnInit {

  @Output()
  tokens: Array<MillToken>;
  suggestions: Suggestion[];
  winner: Player;

  constructor(private mill: MillService, private _iterableDiffers: IterableDiffers) {
  }

  ngOnInit() {
    this.tokens = null;
    this.winner = null;
    this.suggestions = null;

    this.suggestions = this.mill.start({ 'players': [new Player('#FF5200', 'Red'), new Player('#F5CD6D', 'Yellow')] });
    this.tokens = this.mill.getTokens();
  }

  restartGame() {
    this.ngOnInit();
  }

  onSuggestionClicked(suggestion: Suggestion) {
    this.suggestions = this.mill.executeNextTurn(suggestion);

    this.tokens = this.mill.getTokens();


    if (this.mill.isGameFinished()) {
      this.winner = this.mill.getWinner();
      this.suggestions = null;
    }
  }

  trackById(index: number, token: MillToken) {
    return token.id;
  }
}
