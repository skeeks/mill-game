import { Player } from './player';
import { Coordinate } from "./coordinate";

export class MillToken {
    public id;
    private static COUNTER = 0;
    constructor(public player: Player, public coordinate: Coordinate) {
        this.id = MillToken.COUNTER++;
    }

    public equal(other: MillToken): boolean {
        return other && this.id === other.id && this.player && this.player.equal(other.player) && this.coordinate && this.coordinate.equal(other.coordinate);
    }

    private guid() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
          s4() + '-' + s4() + s4() + s4();
      }
}