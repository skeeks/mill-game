import { Coordinate } from "./coordinate";
import { Player } from "./player";
import { SuggestionType } from "./suggestion-type";
import { MillToken } from "./mill-token";


export class Suggestion {

    public constructor(public coordinate : Coordinate, public player : Player, public type : SuggestionType, public origin?: MillToken){

    }
}