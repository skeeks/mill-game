import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MillGameComponent } from './mill-game/mill-game.component';
import { MenuComponent } from './menu/menu.component';
import { CreditsComponent } from './credits/credits.component';

const routes: Routes = [
  { path: '', redirectTo: '/menu', pathMatch: 'full' },
  { path: 'mill-game', component: MillGameComponent },
  { path: 'menu', component: MenuComponent },
  { path: 'credits', component: CreditsComponent }
];
 

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
