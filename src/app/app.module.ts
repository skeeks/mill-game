import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { MenuComponent } from './menu/menu.component';
import { MillGameComponent } from './mill-game/mill-game.component';
import { TokenDirective } from './token.directive';
import { MillService } from './mill-game/mill.service';
import { SuggestionDirective } from './suggestion.directive';
import { CreditsComponent } from './credits/credits.component';


@NgModule({
  declarations: [
    AppComponent,
    MillGameComponent,
    MenuComponent,
    TokenDirective,
    SuggestionDirective,
    CreditsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [
    MillService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
