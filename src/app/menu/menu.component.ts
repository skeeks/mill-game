import { Component, OnInit } from '@angular/core';
import { routerAnimation } from '../animations/routerAnimation';

@Component({
  selector: 'mill-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  animations: [
    routerAnimation
  ]
})
export class MenuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
