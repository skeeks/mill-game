@echo off
echo Web App Build Process started... 
:: Build the Angular Web App
call ng build --prod

echo 1/2 Web App Compiled

:: Remove existing folder
rmdir ..\mill-game-app\www /s /q
:: Move the compiled app into the cordova folder
xcopy dist ..\mill-game-app\www /SI
echo 2/2 Web App transferred to Cordova Compile Area

echo Releaseing Web App finished